from flask import Flask
from flask_bootstrap import Bootstrap

from project.config import get_config


bootstrap = Bootstrap()


def create_app(env="development", dotenv_path=None):
    app = Flask(__name__)

    config = get_config(env, dotenv_path=dotenv_path)

    app.config.from_object(config)

    bootstrap.init_app(app)

    from project.blueprints.errors import bp as errors_bp

    app.register_blueprint(errors_bp)

    from project.blueprints.index import bp as index_bp

    app.register_blueprint(index_bp)

    return app
