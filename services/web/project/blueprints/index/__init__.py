from flask import Blueprint

bp = Blueprint('index', __name__)

from project.blueprints.index import routes  # noqa: E402 F401
