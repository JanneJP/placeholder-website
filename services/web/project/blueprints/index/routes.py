from flask import render_template, jsonify

from project.blueprints.index import bp


@bp.route('/')
@bp.route('/index')
def index():
    return render_template('index/index.html', title='Home')


@bp.route('/ping')
def ping():
    return jsonify({"ping": "pong"}), 200
