import os

from dotenv import load_dotenv


basedir = os.path.abspath(os.path.dirname(__file__))


def get_config(env="development", dotenv_path=None):
    configs = {
        "development": DevelopmentConfig,
        "testing": TestingConfig,
        "production": ProductionConfig
    }

    if dotenv_path:
        load_dotenv(dotenv_path=dotenv_path)
    else:
        load_dotenv()

    env_config = os.environ.get('FLASK_ENV')

    config_name = env_config if env_config else env if env else "development"

    config = configs.get(config_name)

    config = config()

    return config


class Config(object):
    DEBUG = False
    TESTING = False
    SECRET_KEY = os.environ.get('SECRET_KEY')


class DevelopmentConfig(Config):
    DEBUG = True


class TestingConfig(Config):
    TESTING = True


class ProductionConfig(Config):
    pass
