import pytest

from project import create_app


@pytest.fixture(scope='module')
def test_client():
    flask_app = create_app("testing", dotenv_path="../../.env.test")

    assert flask_app.config["TESTING"] is True

    # assert flask_app.config["FLASK_ENV"] == "testing"

    testing_client = flask_app.test_client()

    ctx = flask_app.app_context()
    ctx.push()

    yield testing_client

    ctx.pop()
