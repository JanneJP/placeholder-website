from fixtures import test_client  # noqa: F401


def test_home_page(test_client):  # noqa: F811
    response = test_client.get('/')

    assert response.status_code == 200


def test_ping(test_client):  # noqa: F811
    response = test_client.get('/ping')

    assert response.status_code == 200
    assert response.json == {"ping": "pong"}
